package test;
import java.util.Scanner;

public class Demineur {

static char tabBufferSpace[][] = new char[10][10];
static char tabJeu[][] = new char[10][10];
static char tabVisu[][] = new char[10][10];
static int letter,number,mines,counter = 0;

	public static void main (String[] args) {

	char x = 'X';
	char bufferMines='0';		
	int min = 1,max = 100,complex=0,random = min + (int)(Math.random()*((max-min)+1));	

	try (Scanner sc = new Scanner(System.in)) {
		while (counter<10) {		
			for (int i=0;i<10;i++) {
				tabJeu[counter][i] = '.';
				tabVisu[counter][i] = x;
				tabBufferSpace[counter][i]=' ';
			}
			counter++;
		}


		// CHOIX DE LA DIFFICULTE

		int choix=0;
		while (choix==0) {
		System.out.println();		
		System.out.println("Bonjour et bienvenu sur notre Demineur !");
		System.out.print("Choix de la complexite, combien voulez-vous de mines ? (5/20): "); 	
		complex = sc.nextInt();		
		System.out.println();

			do {
				if (complex<5 || complex>20) {
				System.out.println("Erreur, veuillez saisir un chiffre entre 5 et 20:");
				complex = sc.nextInt();	
				System.out.println();
				}

				else {
				choix=1;	
				}	

			} while (choix==0);		
		}


		// AFFICHAGE tabVisu (PRESENTATION)

		afficheTabVisu();
		System.out.println();


		// REMPLISSAGE X tabJeu

		while (counter<10) {		
			for (int i=0;i<10;i++) {
			tabJeu[counter][i] = x;
			}
		counter++;
		}


		// MINAGE DU TERRAIN

		counter = 0;			
		while (complex>0) {	

			for (int i=0;i<10;i++) {	
			random = min + (int)(Math.random()*((max-min)+1)) ;

				if (random==90 && tabJeu[counter][i] != 'O') {
				tabJeu[counter][i] = 'O';
				complex--;
				}				
			}					
			counter++;

			if (counter==10) {
			counter=0;
			}			
		}

		// BOUCLE DE BALAYAGE

		counter = 0;
		while (counter<10) {					
			for (int i=0;i<10;i++) {																	
				bufferMines='0';		


		// BALAYAGE DU TERRAIN (SCAN EMPLACEMENT MINES)

			if (counter<10 && counter>-1 && i<10 && i>-1) {		

				if (tabJeu[counter][i] != 'O') {											

				if (counter<9) {							
					if (tabJeu[counter+1][i] == 'O') {
					bufferMines++;
					}
				}	

				if (counter>0) {						
					if (tabJeu[counter-1][i] == 'O') {
					bufferMines++;
					}
				}

				if (i<9) {								
					if (tabJeu[counter][i+1] == 'O') {
					bufferMines++;
					}
				}	

				if (i>0) {						
					if (tabJeu[counter][i-1] == 'O') {
					bufferMines++;
					}
				}	

				if (counter<9 && i<9) {							
					if (tabJeu[counter+1][i+1] == 'O') {
					bufferMines++;
					}
				}				

				if (counter<9  && i>0) {						
					if (tabJeu[counter+1][i-1] == 'O') {
					bufferMines++;
					}
				}

				if (counter>0 && i<9) {							
					if (tabJeu[counter-1][i+1] == 'O') {
					bufferMines++;
					}
				}

				if (counter>0 && i>0) {						
					if (tabJeu[counter-1][i-1] == 'O') {
					bufferMines++;
					}	
				}

				if (bufferMines>'0') {
				tabJeu[counter][i] = bufferMines;
				}						
				}					
			}																					
			}
					counter++;													
		}	


		// *** BOUCLE DE SAISIE*** 


		int select=100;
		while (select>1) {


		// SELECTION COORDONNEES

			int siNonX=0;
			do {

				choix=0;
				while (choix==0) {
				System.out.print("Saisir un chiffre (0=A,1=B,2=C,3=D,4=E,5=F,6=G,7=H,8=I,9=J): ");						
				letter = sc.nextInt();

					do {
						if (letter<0 || letter>9) {
						System.out.println();
						System.out.print("Erreur, veuillez saisir un chiffre entre 0 et 9: ");
						letter = sc.nextInt();	
						System.out.println();
						}

						else {
						choix=1;	
						}	

					} while (choix==0);		
				}

				System.out.println();	


				choix=0;
				while (choix==0) {				
				System.out.print("Saisir un Chiffre (0-9): ");
				number = sc.nextInt();

					do {
						if (number<0 || number>9) {
						System.out.println();
						System.out.print("Erreur, veuillez saisir un chiffre entre 0 et 9: ");
						number = sc.nextInt();	
						System.out.println();
						}

						else {
						choix=1;	
						}	

					} while (choix==0);		
				}				

				System.out.println();	

					if (tabVisu[number][letter] != 'X') {
					System.out.println("!!! Faite un autre choix (X) !!!");
					System.out.println();
					}

					else {
					siNonX=1;	
					}

			} while (siNonX==0);


		// TRAITEMENT

		tabVisu[number][letter] = tabJeu[number][letter];


					// PERDU (EJECTION DE LA BOUCLE DE TRAITEMENT)

					if (tabJeu[number][letter] =='O') {
						System.out.println("Vous avez perdu !");
						System.out.println();


						// NETTOYAGE POST AFFICHAGE (tabJeu)

						counter = 0;	
						while (counter<10) {																				
							for (int i=0;i<10;i++) {	
								if (tabJeu[counter][i] == '.') {				
								tabJeu[counter][i] = ' ';			
								}	
							}		
						counter++;
						}	


						// AFFICHAGE TABLEAU FINAL (tabJeu)

						afficheTabJeu();							
						System.out.println();   									
						break;			
						}


		// EXPLORER (RECHERCHE DES ZONES VIDE A DECOUVRIR)

		if (tabJeu[number][letter] =='.') {

			if (number<10 && number>-1 && letter<10 && letter>-1) {																					
			explorer(number, letter, tabJeu);


				// RECHERCHE DES ADRESSES DANS tabBufferSpace POUR TESTER LES 8 CASES ADJACENTES A CETTE ADRESSE DANS tabJeu (MULTIPASSES)

				// PASSE 1

				positiveBruteforce();

				// PASSE 2

				negativeBruteforce();

				// PASSE 3

				positiveBruteforce();

				// PASSE 4

				negativeBruteforce();

				// PASSE 5

				positiveBruteforce();				


		// RECHERCHE DES "TROUS" DANS tabBufferSpace PUIS DES CHIFFRES CORRESPONDANT DANS tabJeu AFIN DE LOCALISER LES CHIFFRES DANS tabJeu A AFFICHER DANS tabVisu

		counter = 0;	
		while (counter<10) {																	
			for (int i=0;i<10;i++) {
				if (tabBufferSpace[counter][i] != 'A') {

					if (tabJeu[counter][i] != 'O') { 

						if (tabJeu[counter][i] == '1') {
						tabVisu[counter][i] = tabJeu[counter][i];
						}

						if (tabJeu[counter][i] == '2') {
						tabVisu[counter][i] = tabJeu[counter][i];
						}

						if (tabJeu[counter][i] == '3') {
						tabVisu[counter][i] = tabJeu[counter][i];
						}

						if (tabJeu[counter][i] == '4') {
						tabVisu[counter][i] = tabJeu[counter][i];
						}

						if (tabJeu[counter][i] == '5') {
						tabVisu[counter][i] = tabJeu[counter][i];
						}

						if (tabJeu[counter][i] == '6') {
						tabVisu[counter][i] = tabJeu[counter][i];
						}

						if (tabJeu[counter][i] == '7') {
						tabVisu[counter][i] = tabJeu[counter][i];
						}

						if (tabJeu[counter][i] == '8') {
						tabVisu[counter][i] = tabJeu[counter][i];
						}
					}
				}
			}		
		counter++;
		}

			}	// FERMETURE 267				
		} 		// FERMETURE 265


		// SI CHIFFRE

		else {
		select--;
		}


				// NETTOYAGE POST AFFICHAGE (tabVisu)

				counter = 0;	
				while (counter<10) {																	

					for (int i=0;i<10;i++) {

						if (tabBufferSpace[counter][i] == 'A') {				
						tabVisu[counter][i] = '.';			
						}		
					}		
				counter++;
				}	


		// ACTUALISTION AFFICHAGE (tabVisu)

		System.out.println();		
		System.out.println("Actualisation du terrain:");
		System.out.println();
		afficheTabVisu();		
		System.out.println();


				// NETTOYAGE POST AFFICHAGE (tabJeu)

				counter = 0;	
				while (counter<10) {																	

					for (int i=0;i<10;i++) {

						if (tabJeu[counter][i] == 'A') {				
						tabJeu[counter][i] = '.';			
						}				
					}		
				counter++;
				}	


					// GAGNER

					int counterX='0';
					int counterO='0';

					counter = 0;
					while (counter<10) {			

						for (int i=0;i<10;i++) {

							if (tabVisu[counter][i]=='X') {
							counterX++;
							}

							if (tabJeu[counter][i]=='O') {
							counterO++;
							}						
						}				
					counter++;

							if (counter==10) {

								if (counterX==counterO) {
								System.out.println("Vous avez gagnez !");
								System.out.println();
								afficheTabJeu();
								System.out.println();
								select=0;
								}
							}
					}												
		select--;				
		}
	}			
   }


											// ***METHODES***

		public static void tabAZ() {   
			char tab_A_J[] = {'A','B','C','D','E','F','G','H','I','J'};

			for (int i=0;i<tab_A_J.length;i++) {
			System.out.print(tab_A_J[i]);
			}
		}

		public static void afficheTabVisu() {   

			System.out.print(" " + " ");									
			tabAZ();
			System.out.println();
			System.out.println();

			counter = 0;
			while (counter<10) {
			System.out.print(counter + " ");									

				for (int i=0;i<10;i++) {
				System.out.print(tabVisu[counter][i]);
				}		

			System.out.println();
			counter++;
			}		
		}

		public static void afficheTabJeu() {   

			System.out.print(" " + " ");									
			tabAZ();
			System.out.println();
			System.out.println();

			counter = 0;	
			while (counter<10) {
			System.out.print(counter + " ");			

				for (int i=0;i<10;i++) {
				System.out.print(tabJeu[counter][i]);
				}

			System.out.println();
			counter++;
			}		
		}			

		public static void positiveBruteforce() {   

				counter = 0;
				while (counter<10) {		
				for (int i=0;i<10;i++) {

					if (tabBufferSpace[counter][i] == 'A') {										
					tabJeu[counter][i] = tabBufferSpace[counter][i];							

						if (number<10 && number>-1 && letter<10 && letter>-1) {										
							explorer(counter, i, tabJeu);									
							tabBufferSpace[counter][i] = '*';		

							if (counter<10) {
							tabBufferSpace[counter][i] = 'A';
							}									
						}	
					}
				}
			counter++;
			}					
		}

		public static void negativeBruteforce() {   

			counter = 9;
			while (counter>0) {		
				for (int i=0;i<10;i++) {

					if (tabBufferSpace[counter][i] == 'A') {										
					tabJeu[counter][i] = tabBufferSpace[counter][i];							

						if (number<10 && number>-1 && letter<10 && letter>-1) {										
							explorer(counter, i, tabJeu);									
							tabBufferSpace[counter][i] = '*';		

								if (counter<10) {
								tabBufferSpace[counter][i] = 'A';
								}
						}	
					}
				}
			counter--;
			}				
		}			

		public static void explorer(int number, int letter, char tabJeu[][]) {

			if (number<9) {							
				if (tabJeu[number+1][letter] == '.') {
				tabBufferSpace[number+1][letter]='A';
				}
			}	

			if (number>0) {						
				if (tabJeu[number-1][letter] == '.') {
				tabBufferSpace[number-1][letter]='A';
				}
			}

			if (letter<9) {								
				if (tabJeu[number][letter+1] == '.') {
				tabBufferSpace[number][letter+1]='A';
				}
			}	

			if (letter>0) {						
				if (tabJeu[number][letter-1] == '.') {
				tabBufferSpace[number][letter-1]='A';
				}
			}	

			if (number<9 && letter<9) {							
				if (tabJeu[number+1][letter+1] == '.') {
				tabBufferSpace[number+1][letter+1]='A';
				}
			}				

			if (number<9  && letter>0) {						
				if (tabJeu[number+1][letter-1] == '.') {
				tabBufferSpace[number+1][letter-1]='A';
				}
			}

			if (number>0 && letter<9) {							
				if (tabJeu[number-1][letter+1] == '.') {
				tabBufferSpace[number-1][letter+1]='A';
				}
			}

			if (number>0 && letter>0) {						
				if (tabJeu[number-1][letter-1] == '.') {
				tabBufferSpace[number-1][letter-1]='A';
				}	
			}
		}
}