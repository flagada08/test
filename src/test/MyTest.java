package test;

import java.util.Scanner;

public class MyTest {

	public static void main(String[ ] args) {
		
		try (Scanner input = new Scanner(System.in)) {
			int binary = 0;
			int radix = 2;

			while (binary == 0) {

			    System.out.print("Please input a valid binary: ");

			    if (input.hasNextInt(radix)) {
			        binary = input.nextInt(radix);
			    } else {
			        System.out.println("Not a Valid Binary");
			    }
			    input.nextLine();
			}

			System.out.print("Converted Binary: " + binary);
		}
    }
}
