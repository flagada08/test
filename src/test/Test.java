package test;

import java.util.Scanner;

public class Test {

	protected static final Scanner clavier = new Scanner(System.in);
		
	public static int nextInt(String message) {
		while (true)
		{
			System.out.print(message);
						
			try {
				return Integer.parseInt(clavier.nextLine());
			}
			catch (Exception e) {
				System.out.println("Error");
			}
		}
	}
		
	public static void main(String...args) {
		int a = nextInt("a = ");
		int b = nextInt("b = ");
		System.out.println("a+b="+(a+b));
	}

}
