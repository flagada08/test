package test;

public class TestTab2D {
	
    public static void main(String[] args) {
        int[][] tab2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
    
        int j, i = 0;
        for (int[] tab1D : tab2D) {
            j = 0;
            for (int val : tab1D) {
                System.out.println("[" + i + "][" + j + "] = " + val);
                j++;
            }
            i++;
        }
    }
}
